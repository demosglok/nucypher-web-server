var express = require('express');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const cors = require('cors');

var indexRouter = require('./routes/index');
var nucypherRouter = require('./routes/nucypher');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors({credentials: true, origin: true}));

app.use('/', indexRouter);
app.use('/nu', nucypherRouter);

module.exports = app;
