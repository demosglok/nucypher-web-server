const {PythonShell}= require('python-shell');

const pyshell = new PythonShell('run', {
  mode: 'json',
  parser(data) {
    try {
      return JSON.parse(data);
    } catch (ex) {
      return {error: ex.message || 'failed to parse'};
    }
  },
  pythonPath: 'pipenv',
  args: ['python', './nucypher-python/rpc.py']
});

const resolvers = []

pyshell.on('message', (message) => {
  if(resolvers.length > 0) {
    let {resolve, reject} = resolvers.shift();
    if(typeof resolve === 'function') {
      if(message.status === 'success') {
        resolve(message.result);
      } else {
        reject(message.error || 'RPC error');
      }
    } else {
      console.log('unexpected promise in queue', resolver);
    }
  }
})

const send = (msg) => {
  pyshell.send(msg);
  return new Promise((resolve, reject) => {
    resolvers.push({resolve, reject});
  })
}
/*
pyshell.end((err, code, signal) => {
  console.log('pyshell stopped, err=', err, ', code=', code,', signal=', signal);
});
*/

module.exports = {
  /**
  * @desc generates keypair, private and public key
  * @return promise(object) - with privkey and pubkey fields
  */
  genkey() {
    return send({command: 'genkey'});
  },

  /**
  * @desc encrypts data (textual for the moment) with publick key
  * @param string pubkey - public key to use for encryption
  * @param string data - data to be encrypted
  * @return promise(object) - with ciphertext and capsule (to be used later)
  */
  encrypt(pubkey, data) { //only text data for the moment
    return send({command: 'encrypt', params: {pubkey, data}});
  },

  /**
  * @desc grants permission for specific user to decrypt data
  * @param string alice_privkey - alice private key, alice - persont granting permissions
  * @param string alice_signing_privkey - alice signing private key (separate private key from first one)
  * @param string bob_pubkey - bobs public key, bob - person to have permission to decrypt data
  * @return promise(string) - policy_id , id of policy with permissions
  */
  grant(alice_privkey, alice_signing_privkey, bob_pubkey) {
    return send({command: 'grant', params: {
      alice_privkey,
      alice_signer: alice_signing_privkey,
      bob_pubkey
    }});
  },

  /**
  * @desc reencrypts data, returning array of fragments. usually its done automatically by network
  * @param string alice_pubkey - alice public key
  * @param string alice_signing_pubkey - alice signing public key (separate public key from first one)
  * @param string bob_pubkey - bobs public key
  * @param string bob_capsule - capsule, returned from encryption step, used to gather fragments
  * @param string policy_id - id of policy, returned by grant method
  * @return promise(array) - array of fragments, necessary for decryption
  */
  reencrypt(alice_pubkey, alice_signing_pubkey, bob_pubkey, bob_capsule, policy_id) {
    return send({command: 'reencrypt', params: {
      alice_pubkey,
      alice_signing_pubkey,
      bob_pubkey,
      bob_capsule,
      policy_id,
    }});
  },

  /**
  * @desc decrypts data, returning text
  * @param string alice_pubkey - alice public key
  * @param string alice_signing_pubkey - alice signing public key (separate public key from first one)
  * @param string bob_privkey - bobs private key,
  * @param string bob_capsule - capsule, returned from encryption step, used to gather fragments
  * @param string cfrags - array of fragments
  * @param string ciphertext - encrypted text to be decrypted
  * @return promise(string) - decrypted text
  */
  decrypt(alice_pubkey, alice_signing_pubkey, bob_privkey, bob_capsule, cfrags, ciphertext) {
    return send({command: 'decrypt', params: {
      alice_pubkey,
      alice_signing_pubkey,
      bob_privkey,
      bob_capsule,
      cfrags,
      ciphertext
    }});
  },

  /**
  * @desc revokes access by policy id
  * @param string policy_id - policy_id returned by grant
  * @return promise(void)
  */
  revoke(policy_id) {
    return send({command: 'revoke', params: {
      policy_id
    }});
  },
}
/*
(async() => {
  const alice_keys = await send({command: 'genkey'});
  console.log('alice_keys', alice_keys);
  const alice_signing_keys = await send({command: 'genkey'})
  console.log('alice signing keys', alice_signing_keys);
  const bob_keys = await send({command: 'genkey'});
  console.log('bob_keys', bob_keys);
  const plaintext = 'some interesting text';
  const encrypted = await send({command: 'encrypt', params: {pubkey: alice_keys.pubkey, data: plaintext}});
  console.log('plaintext', plaintext);
  console.log('encrypted', encrypted);
  const policy_id = await send({command: 'grant', params: {
    alice_privkey: alice_keys.privkey,
    alice_signer: alice_signing_keys.privkey,
    bob_pubkey: bob_keys.pubkey
  }});
  console.log('policy_id', policy_id);
  const cfrags = await send({command: 'reencrypt', params: {
    alice_pubkey: alice_keys.pubkey,
    alice_signing_pubkey: alice_signing_keys.pubkey,
    bob_pubkey: bob_keys.pubkey,
    bob_capsule: encrypted.capsule,
    policy_id,
  }});
  console.log('cfrags', cfrags);
  const decrypted = await send({command: 'decrypt', params: {
    alice_pubkey: alice_keys.pubkey,
    alice_signing_pubkey: alice_signing_keys.pubkey,
    bob_privkey: bob_keys.privkey,
    bob_capsule: encrypted.capsule,
    cfrags,
    ciphertext: encrypted.ciphertext
  }})
  console.log('decrypted', decrypted)
})().catch(ex => console.log('sequence error', ex.message));
*/
