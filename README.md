# Sample nodejs webserver exposing nucypher functions via REST API



## To Use
you need python3.6 or later
```bash
# Clone this repository
git clone https://gitlab.com/demosglok/nucypher-web-server
# Go into the repository
cd nucypher-web-server
# Install dependencies
npm install
# setup python virtualenv
./python-init.sh
# Run the app
npm start
```

app will be run on 3000 port