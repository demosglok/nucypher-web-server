const express = require('express');
const nu = require('../nucypher-nodejs');

const router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('sample nucypher API');
});

router.get('/genkey', async (req, res, next) => {
  try {
    const keys = await nu.genkey();
    res.json({success: true, keys });
  } catch(ex) {
    res.json({success: false, error: ex.message});
  }
})
router.post('/encrypt', async (req, res, next) => {
  try {
    if(req.body.pubkey && req.body.data) {
      const encrypted = await nu.encrypt(req.body.pubkey, req.body.data);
      res.json({success: true, encrypted });
    } else {
      res.json({success: false, error: 'missing pubkey or data'});
    }
  } catch(ex) {
    res.json({success: false, error: ex.message});
  }
})
router.post('/grant', async (req, res, next) => {
  try {
    if(req.body.granter_privkey && req.body.granter_signkey && req.body.granted_pubkey) {
      const policy_id = await nu.grant(req.body.granter_privkey, req.body.granter_signkey, req.body.granted_pubkey);
      res.json({success: true, policy_id });
    } else {
      res.json({success: false, error: 'missing granter_privkey or granter_signkey or granted_pubkey'});
    }
  } catch(ex) {
    res.json({success: false, error: ex.message});
  }
})
router.post('/reencrypt', async (req, res, next) => {
  try {
    if(req.body.source_pubkey &&
      req.body.source_sign_pubkey &&
      req.body.target_pubkey &&
      req.body.capsule &&
      req.body.policy_id) {
      const cfrags = await nu.reencrypt(
        req.body.source_pubkey,
        req.body.source_sign_pubkey,
        req.body.target_pubkey,
        req.body.capsule,
        req.body.policy_id
      );
      res.json({success: true, cfrags });
    } else {
      res.json({success: false, error: 'incomplete input data'})
    }
  } catch(ex) {
    res.json({success: false, error: ex.message});
  }
})
router.post('/decrypt', async (req, res, next) => {
  try {
    if(req.body.source_pubkey &&
      req.body.source_sign_pubkey &&
      req.body.target_privkey &&
      req.body.capsule &&
      req.body.cfrags &&
      req.body.encrypted_data) {
      const decrypted = await nu.decrypt(
        req.body.source_pubkey,
        req.body.source_sign_pubkey,
        req.body.target_privkey,
        req.body.capsule,
        req.body.cfrags,
        req.body.encrypted_data
      );
      res.json({success: true, decrypted });
    } else {
      res.json({success: false, error: 'incomplete input data'})
    }
  } catch(ex) {
    res.json({success: false, error: ex.message});
  }
})
router.post('/revoke', async (req, res, next) => {
  try {
    if(req.body.policy_id) {
      const keys = await nu.revoke(policy_id);
      res.json({success: true});
    }
  } catch(ex) {
    res.json({success: false, error: ex.message});
  }
})
module.exports = router;
